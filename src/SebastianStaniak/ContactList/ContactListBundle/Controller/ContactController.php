<?php

namespace SebastianStaniak\ContactList\ContactListBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use SebastianStaniak\ContactList\ContactListBundle\Entity\Contact;
use SebastianStaniak\ContactList\ContactListBundle\Form\ContactType;

/**
 * Contact controller.
 *
 */
class ContactController extends Controller
{

    /**
     * Lists all Contact entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $request->query->get('order', 'firstName'); 
        $direction = $request->query->get('direction', 'ASC'); 
        
        $fields = [
            'firstName', 
            'lastName', 
            'phoneNumber', 
            'email', 
            'address', 
            'city', 
            'zip', 
            'isFriend'
        ];

        $sort   = [];
        foreach ($fields as $field) {
            if ($order === $field) {
                $sort[$field] = '?order=' . $field . '&direction=DESC';
            } else {
                $sort[$field] = '?order=' . $field . '&direction=ASC';
            }
        }

        $entities = $em->getRepository('ContactListBundle:Contact')->findBy([], [$order => $direction]);
        $counted = $em->getRepository('ContactListBundle:Contact')->countAll();

        return $this->render('ContactListBundle:Contact:index.html.twig', [
            'entities' => $entities,
            'sort'     => $sort,
            'counted'  => $counted
        ]);
    }
    
    /**
     * Lists all Contact entities filtered by _POST[search].
     *
     */
    public function searchAction(Request $request)
    {
        $searchName = $request->request->get('search');

        $order = $request->query->get('order', 'firstName'); 
        $direction = $request->query->get('direction', 'ASC'); 
        
        $fields = [
            'firstName', 
            'lastName', 
            'phoneNumber', 
            'email', 
            'address', 
            'city', 
            'zip', 
            'isFriend'
        ];

        
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ContactListBundle:Contact')->searchByFirstName($searchName, [$order => $direction]);
        $counted = $em->getRepository('ContactListBundle:Contact')->countAll();
        
                $sort   = [];
        foreach ($fields as $field) {
            if ($order === $field) {
                $sort[$field] = '?order=' . $field . '&direction=DESC';
            } else {
                $sort[$field] = '?order=' . $field . '&direction=ASC';
            }
        }
        
        return $this->render('ContactListBundle:Contact:index.html.twig', [
            'entities' => $entities,
            'sort'     => $sort,
            'counted'  => $counted
        ]);
    }
    /**
     * Creates a new Contact entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Contact();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('_show', ['id' => $entity->getId()]));
        }

        return $this->render('ContactListBundle:Contact:new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a form to create a Contact entity.
     *
     * @param Contact $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Contact $entity)
    {
        $form = $this->createForm(new ContactType(), $entity, [
            'action' => $this->generateUrl('_create'),
            'method' => 'POST',
        ]);

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Displays a form to create a new Contact entity.
     *
     */
    public function newAction()
    {
        $entity = new Contact();
        $form   = $this->createCreateForm($entity);

        return $this->render('ContactListBundle:Contact:new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Contact entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ContactListBundle:Contact')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contact entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ContactListBundle:Contact:show.html.twig', [
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Contact entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ContactListBundle:Contact')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contact entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ContactListBundle:Contact:edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
    * Creates a form to edit a Contact entity.
    *
    * @param Contact $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Contact $entity)
    {
        $form = $this->createForm(new ContactType(), $entity, [
            'action' => $this->generateUrl('_update', ['id' => $entity->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
    /**
     * Edits an existing Contact entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ContactListBundle:Contact')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contact entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('_edit', ['id' => $id]));
        }

        return $this->render('ContactListBundle:Contact:edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }
    /**
     * Deletes a Contact entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ContactListBundle:Contact')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contact entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl(''));
    }

    /**
     * Creates a form to delete a Contact entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('_delete', ['id' => $id]))
            ->setMethod('DELETE')
    ->add('submit', 'submit', ['label' => 'Delete'])
            ->getForm()
        ;
    }
}
