To install this application: 

 * Install all dependicies using "composer install"
 * Create your own parameters.yml based on parameters.yml.dist filled with MySQL connection params,
    or if you prefer SQLite like me set pdo_sqlite as doctrine-dbal-driver in config.yml and set right path to file 
 * Run "php app/console doctrine:schema:update --force" to generate SQL schema
 * Host app via Apache, Nginx or Symfony build-in server
 * Enjoy